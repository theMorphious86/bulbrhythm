﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Persistence
{
    /*public class DefaultValue
    {
        public string key;
        public object value;

        DefaultValue(string key, object value)
        {
            this.key = key;
            this.value = value;
        }
    }*/

    public const string key_inputOffset = "inputOffset";
    public const string key_visualOffset = "visualOffset";
    public const string key_musicVolume = "musicVolume";
    public const string key_soundVolume = "soundVolume";
    public const string key_resolution = "resolution";

    public static readonly Dictionary<string, object> defaults = new Dictionary<string, object>
    {
        { key_inputOffset, 0 },
        { key_visualOffset, 0 },
        { key_musicVolume, 100 },
        { key_soundVolume, 100 },
        { key_resolution, 1 }
    };

    public static int GetInt(string key)
    {
        return PlayerPrefs.GetInt(key, (int)defaults[key]);
    }

    public static float GetFloat(string key)
    {
        return PlayerPrefs.GetFloat(key, (float)defaults[key]);
    }

    public static string GetString(string key)
    {
        return PlayerPrefs.GetString(key, (string)defaults[key]);
    }

    public static void SetInt(string key, int value)
    {
        PlayerPrefs.SetInt(key, value);
    }

    public static void SetFloat(string key, float value)
    {
        PlayerPrefs.SetFloat(key, value);
    }

    public static void SetString(string key, string value)
    {
        PlayerPrefs.SetString(key, value);
    }

    public static void Save()
    {
        PlayerPrefs.Save();
    }
}
