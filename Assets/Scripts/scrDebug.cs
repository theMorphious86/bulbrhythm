﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;


public class scrDebug : SingletonBehaviour<scrDebug>
{
    public TextMeshProUGUI debugText;
    public Slider debugSlider;
    public TextMeshProUGUI debugRating;

    void Start()
    {

    }

    void Update()
    {
        var music = scrMusic.instance;

        debugText.text =
            $"Total beat: {Mathf.FloorToInt(music.beat)}" +
            $"\nCrotchet: {music.crotchet:F2}" + $"\nBar: {music.bar}";
    }

    public void PulseTest(string msg)
    {
        debugRating.text = msg;
        debugRating.transform.localScale = new Vector3(1.25f, 1.25f, 1f);
        debugRating.transform.DOScale(1f, 0.1f);
    }
}
