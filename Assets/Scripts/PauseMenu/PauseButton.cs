﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using TMPro;

public class PauseButton : MonoBehaviour
{
    TextMeshProUGUI _text;
    public TextMeshProUGUI text
    {
        get
        {
            if (_text == null) _text = GetComponent<TextMeshProUGUI>();
            return _text;
        }
    }

    [NonSerialized] public Transform leftCursorHolder;
    [NonSerialized] public Transform rightCursorHolder;
    [Header("Generic")]
    public string hoverAction;
    public string enterAction;
    public string leftAction;
    public string rightAction;

    [Header("Setting")]
    public string valueKey;
    public float minValue;
    public float maxValue;
    public float increment;
    public bool isWhole;

    private bool cappedMin;
    private bool cappedMax;
    private float value;
    private string origText;

    void OnEnable()
    {
        if (origText == null) origText = text.text;
        UpdateText(true);
    }

    void UpdateText(bool readFromPersistence = false)
    {
        if (valueKey != "")
        {
            if (readFromPersistence) value = isWhole ? Persistence.GetInt(valueKey) : Persistence.GetFloat(valueKey);
            text.text = origText.Replace("*", value.ToString());

            cappedMin = value - increment < minValue;
            cappedMax = value + increment > maxValue;
        }
        UpdatePositions();
    }

    public void IncrementValue(bool negative = false)
    {
        if ((cappedMin && negative) || (cappedMax && !negative))
        {
            scrSoundManager.instance.NewSound("sndDecrease").IgnorePause().PlaySound();
            return;
        }

        value = value + (negative ? -increment : increment);
        if (isWhole)
            Persistence.SetInt(valueKey, (int)value);
        else
            Persistence.SetFloat(valueKey, value);

        scrSoundManager.instance.NewSound(negative ? "sndNo" : "sndIncrease").IgnorePause().PlaySound();
        UpdateText();
    }

    void UpdatePositions()
    {
        text.ForceMeshUpdate();
        Transform tr = text.transform;
        Vector3 oldScale = tr.localScale;
        tr.localScale = Vector3.one;

        var leftChar = tr.TransformPoint(text.textInfo.characterInfo[0].topLeft);
        var rightChar = tr.TransformPoint(text.textInfo.characterInfo[text.textInfo.characterCount - 1].topRight);
        var leftCursorPos = new Vector2(leftChar.x, tr.position.y);
        var rightCursorPos = new Vector2(rightChar.x, tr.position.y);

        if (leftCursorHolder == null)
        {
            GameObject leftObj = new GameObject("leftCursor");
            GameObject rightObj = new GameObject("rightCursor");

            leftCursorHolder = leftObj.transform;
            leftCursorHolder.parent = transform;
            rightCursorHolder = rightObj.transform;
            rightCursorHolder.parent = transform;
        }

        leftCursorHolder.position = leftCursorPos;
        rightCursorHolder.position = rightCursorPos;
        tr.localScale = oldScale;
    }

    //Bounds bounds = text.textBounds;
    //scrPauseMenu.instance.cursor.position = bounds.max;
    //scrPauseMenu.instance.cursor.position = text.mesh.bounds.center;
    //Debug.Log(text.text);
    //scrPauseMenu.instance.cursor.position = text.transform.position + new Vector3(text.GetRenderedValues(true).x, text.GetRenderedValues(true).y, 0);
    //scrPauseMenu.instance.cursor.position = text.textInfo.lineInfo[0].;
    //scrPauseMenu.instance.cursor.position = text.GetRenderedValues(true);
    //scrPauseMenu.instance.cursor.position = text.transform.position;
    //rightCursorPos = new Vector2(pos.x + text.textInfo.characterInfo[this.text.textInfo.lineInfo[0].lastVisibleCharacterIndex].xAdvance, pos.y);

    /*
    public void UpdatePositions()
    {
        text.ForceMeshUpdate();
        float length = text.textInfo.lineInfo[0].length;
        float offset = 0f;
        Vector3 pos = text.transform.position;
        leftCursorPos = new Vector2(pos.x - (length / 2 + offset), pos.y);
        rightCursorPos = new Vector2(pos.x + (length / 2 + offset), pos.y);
        scrPauseMenu.instance.cursor.position = rightCursorPos;
    }*/
}
