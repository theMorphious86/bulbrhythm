﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class HitMarker : MonoBehaviour
{
    public float beat;
    SpriteRenderer spriteRenderer;


    void Start()
    {
        //DOVirtual.DelayedCall(0.66f, () => Destroy(gameObject));
    }

    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }


    public HitMarker init(float beat)
    {
        this.beat = beat;

        scrMusic mus = scrMusic.instance;

        float degrees = (beat - 0.5f) * 360 / 2 % 360;
        if (degrees > 180) degrees = 180 - degrees + 180;
        var rad = Mathf.Abs(degrees) * Mathf.Deg2Rad;
        var radius = 52f;
        var pos = new Vector2(Mathf.Cos(rad), Mathf.Sin(rad)) * radius;
        transform.localPosition = pos;

        transform.localScale = Vector3.zero;
        transform.DOScale(Vector3.one, 0.2f).SetEase(Ease.OutBack);

        Debug.Log(degrees);

        return this;
    }

    public void SetReady(bool ready)
    {
        Vector3 scale = ready ? Vector3.one : Vector3.one / 2;
        transform.DOScale(scale, 0.2f);

        float alpha = ready ? 1 : 0.5f;
        spriteRenderer.DOFade(alpha, 0.5f);
    }

    public void Disappear()
    {
        transform.DOScale(Vector3.zero, 0.2f).OnComplete(() => Destroy(gameObject));
        //Destroy(gameObject);
    }
}
