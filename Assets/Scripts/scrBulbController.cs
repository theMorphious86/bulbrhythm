﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;
using System;

public class scrBulbController : MonoBehaviour
{
    public Transform samurai;
    public List<Sprite> frames;

    SpriteRenderer spriteRenderer;

    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    void Update()
    {
        scrMusic mus = scrMusic.instance;

        int index = Mathf.FloorToInt((mus.visualBeat * frames.Count / 2) % frames.Count);

        spriteRenderer.sprite = frames[index];

        float degrees = (mus.visualBeat - 0.5f) * 360 / 2 % 360;
        if (degrees > 180) degrees = 180 - degrees + 180;
        var rad = degrees * Mathf.Deg2Rad;
        var radius = 42f;
        var pos = new Vector2(Mathf.Cos(rad), Mathf.Sin(rad)) * radius;
        samurai.localPosition = pos;
    }
}
