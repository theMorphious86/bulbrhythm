﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class scrController : SingletonBehaviour<scrController>
{
    [Header("Configuration")]
    public float inputMargin;
    public float inputOffset;
    public float visualOffset;
    public bool ignoreMultiHits;

    [Header("Components")]

    [Header("Other")]
    public GameObject hitMarkerPrefab;
    public Transform hitMarkerContainer;

    [System.NonSerialized] public bool skipInput;
    List<HitMarker> markers = new List<HitMarker>();

    int nextBeat = 0;

    scrMusic music => scrMusic.instance;

    public List<InputEvent> inputEvents = new List<InputEvent>();

    void Start()
    {
        Play();
    }

    public void Play()
    {
        music.Load();
        music.Play();

        for (float beat = 0f; beat < music.finalBeat; beat += 1f)
        {
            InputEvent ev = new InputEvent();
            ev.Init(music.BeatToBar(beat), music.BeatToCrotchet(beat));
            inputEvents.Add(ev);
        }


        /*for (float beat = 0.5f; beat < music.finalBeat; beat += 1f)
        {
            InputEvent ev = new InputEvent();
            ev.Init(music.BeatToBar(beat), music.BeatToCrotchet(beat));
            inputEvents.Add(ev);
        }*/

        for (float beat = 0.1f; beat < music.finalBeat; beat += 1f)
        {
            InputEvent ev = new InputEvent();
            ev.Init(music.BeatToBar(beat), music.BeatToCrotchet(beat));
            inputEvents.Add(ev);
        }

        for (float beat = 0.2f; beat < music.finalBeat; beat += 1f)
        {
            InputEvent ev = new InputEvent();
            ev.Init(music.BeatToBar(beat), music.BeatToCrotchet(beat));
            inputEvents.Add(ev);
        }

        for (float beat = 0.3f; beat < music.finalBeat; beat += 1f)
        {
            InputEvent ev = new InputEvent();
            ev.Init(music.BeatToBar(beat), music.BeatToCrotchet(beat));
            inputEvents.Add(ev);
        }

        for (float beat = 0.4f; beat < music.finalBeat; beat += 1f)
        {
            InputEvent ev = new InputEvent();
            ev.Init(music.BeatToBar(beat), music.BeatToCrotchet(beat));
            inputEvents.Add(ev);
        }

        inputEvents = inputEvents.OrderBy(x => x.beat).ToList();
    }

    void Update()
    {
        if (scrPauseMenu.instance.paused) return;
        List<InputEvent> eventsToRemove = new List<InputEvent>();

        bool pressed = Input.anyKeyDown && !InvalidInputWasPressed() && !skipInput;

        float marginBeats = music.SecondsToBeats(inputMargin);
        float adjustedBeat = music.beat - music.SecondsToBeats(inputOffset / 1000f);

        int validHits = 0;
        foreach (InputEvent ev in inputEvents)
        {
            if (ev.fired) continue;
            if (music.bar + 1 < ev.bar) break; // ignore events on future bars
            if (adjustedBeat < ev.beat - (marginBeats * 3)) continue; // ignore if event hasn't started yet
            if (adjustedBeat > ev.beat + (marginBeats * 2)) // MISS if too late
            {
                eventsToRemove.Add(ev);
                //Debug.Log("VERY LATE");
                scrSoundManager.instance.NewSound("sndMiss").PlaySound();
                scrDebug.instance.PulseTest("VERY LATE!!");
            }
            else if (pressed)
            {
                float diff = adjustedBeat - ev.beat;
                scrDebug.instance.debugSlider.value = ((diff / (marginBeats * 2)) / 2) + 0.5f;

                if (diff < -marginBeats)
                {
                    //Debug.Log("EARLY");
                    scrSoundManager.instance.NewSound("sndMiss").PlaySound();
                    scrDebug.instance.PulseTest("Early!!");
                }
                else if (diff > marginBeats)
                {
                    //Debug.Log("LATE");
                    scrSoundManager.instance.NewSound("sndMiss").PlaySound();
                    scrDebug.instance.PulseTest("Late!!");
                }
                else
                {
                    //Debug.Log($"CORRECT! Beat: {music.beat}, diff: {diff}");
                    validHits++;
                    scrSoundManager.instance.NewSound("sndHit").PlaySound();
                    scrDebug.instance.PulseTest("Perfect!");
                }
                eventsToRemove.Add(ev);
            }

            if (ignoreMultiHits) break;
        }

        if (pressed && validHits == 0 && eventsToRemove.Count == 0) scrSoundManager.instance.NewSound("sndPop").PlaySound();

        foreach (InputEvent evToRemove in eventsToRemove)
        {
            //inputEvents.Remove(evToRemove);
            evToRemove.fired = true;
        }

        skipInput = false;


        if (music.beat >= nextBeat + 0.5f)
        {
            UpdateMarkers(nextBeat);
            nextBeat += 1;
        }
    }

    public bool InvalidInputWasPressed()
    {
        KeyCode[] invalidKeys =
        {
            KeyCode.Escape,
            KeyCode.F1,
            KeyCode.F2,
            KeyCode.F3,
            KeyCode.F4,
            KeyCode.F5,
            KeyCode.F6,
            KeyCode.F7,
            KeyCode.F8,
            KeyCode.F9,
            KeyCode.F10,
            KeyCode.F11,
            KeyCode.F12,
            KeyCode.LeftWindows,
            KeyCode.RightWindows,
            KeyCode.LeftApple,
            KeyCode.RightApple,
            KeyCode.Print,
            KeyCode.Menu
        };

        foreach (KeyCode key in invalidKeys)
        {
            if (Input.GetKeyDown(key)) return true;
        }

        return false;
    }

    void UpdateMarkers()
    {
        UpdateMarkers(Mathf.RoundToInt(music.beat));
    }

    void UpdateMarkers(int beat)
    {
        bool oddBeat = beat % 2 == 1;
        List<HitMarker> toRemove = new List<HitMarker>();
        foreach (HitMarker marker in markers)
        {
            if (oddBeat)
            {
                InputEvent nextPress = inputEvents.Where(x => x.beat == beat + marker.beat).SingleOrDefault();
                if (marker.beat > 1)
                {
                    if (nextPress == null)
                        toRemove.Add(marker);
                    else
                        marker.SetReady(false);
                }
            }
            else
            {
                InputEvent nextPress = inputEvents.Where(x => x.beat == beat + marker.beat).SingleOrDefault();
                if (marker.beat < 1)
                {
                    if (nextPress == null)
                        toRemove.Add(marker);
                    else
                        marker.SetReady(false);
                }
            }
            //foreach (InputEvent ev in InputEvent)
        }

        foreach (HitMarker marker in toRemove)
        {
            markers.Remove(marker);
            marker.Disappear();
        }

        foreach (InputEvent ev in inputEvents)
        {
            if (ev.beat < beat + 0.5f) continue;
            if (ev.beat > beat + 1.5f) break;

            float cappedBeat = ev.beat % 2;
            HitMarker marker = markers.Where(x => x.beat == cappedBeat).SingleOrDefault();

            if (marker == null)
            {
                marker = Instantiate(hitMarkerPrefab, hitMarkerContainer).GetComponent<HitMarker>();
                marker.init(cappedBeat);

                markers.Add(marker);
            }
            else
            {
                marker.SetReady(true);
            }
        }
    }
}
