﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputEvent
{
    // settings
    public int bar;
    public float crotchet;
    public float margin;
    public bool fired = false;

    // info
    public float beat;

    public void Init(int bar, float crotchet, float margin = -1)
    {
        this.beat = bar * scrMusic.instance.crotchetsPerBar + crotchet;
        if (margin == -1) margin = scrController.instance.inputMargin;

        float beatMargin = scrMusic.instance.SecondsToBeats(margin);
    }
}
