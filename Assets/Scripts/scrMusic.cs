﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scrMusic : SingletonBehaviour<scrMusic>
{
    [Header("Configuration")]
    public string songDirectory;
    public string songFilename;
    public bool isInternalSong;
    public float bpm;
    public int offset;
    public int crotchetsPerBar;
    public float volume;

    [Header("Important")]
    public float bar;
    public float beat;
    public float visualBeat;
    public float crotchet;
    public float time;
    public float sampleTime;
    public float frequency;

    [NonSerialized] public double startTime;
    [NonSerialized] public float finalBeat;
    private AudioSource source;

    void Start()
    {

    }

    void Update()
    {
        //time = (float)(AudioSettings.dspTime - startTime);
        time = source.time;
        beat = time / (60 / bpm) - SecondsToBeats(offset / 1000f);
        bar = BeatToBar(beat);
        crotchet = BeatToCrotchet(beat);

        visualBeat = beat - scrController.instance.visualOffset / 1000f;

        source.volume = volume / 100;
    }

    //loading the song takes time, so make sure to call this before calling Play()
    public void Load()
    {
        if (string.IsNullOrEmpty(songFilename)) return;

        source = GetComponent<AudioSource>();

        if (isInternalSong)
        {
            var songPath = "Sounds/Music/" + songFilename;
            source.clip = Resources.Load<AudioClip>(songPath);
        }
        else
        {
            if (string.IsNullOrEmpty(songDirectory)) return;
        }
        frequency = source.clip.frequency;
        finalBeat = source.clip.length / (60 / bpm);
    }

    public void Play()
    {
        startTime = AudioSettings.dspTime;
        if (offset > 0)
        {
            source.Play();
            source.time = offset / 1000;
        }
        else
        {
            source.PlayDelayed(-offset / 1000);
        }
    }

    public int BeatToBar(float beat)
    {
        int bar = Mathf.FloorToInt(beat / crotchetsPerBar);
        return bar;
    }

    public float BeatToCrotchet(float beat)
    {
        int bar = BeatToBar(beat);
        float crotchet = beat - (bar * crotchetsPerBar);
        return crotchet;
    }

    public float SecondsToBeats(float seconds)
    {
        float beats = seconds * (bpm / 60);
        return beats;
    }
}
