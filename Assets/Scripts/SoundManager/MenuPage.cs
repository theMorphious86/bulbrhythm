﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class MenuPage : MonoBehaviour
{
    List<PauseButton> _buttons;
    public List<PauseButton> buttons
    {
        get
        {
            if (_buttons == null) UpdateButtons();
            return _buttons;
        }
    }

    public void UpdateButtons()
    {
        _buttons = GetComponentsInChildren<PauseButton>().ToList();
    }

    public PauseButton GetButtonByAction(string action)
    {
        foreach (PauseButton pb in buttons)
        {
            if (pb.enterAction == action) return pb;
        }
        return null;
    }

    public int GetIndexByAction(string action)
    {
        return buttons.IndexOf(GetButtonByAction(action));
    }
}
