﻿using UnityEngine;

public class Sound : MonoBehaviour
{
    private bool scheduled; //should this sound be played instantly or some time later
    private float scheduleTime; //when will the sound be played

    public AudioClip audioClip;

    private bool hasPlayed;
    private AudioSource source;
    private float volume = 1f;

    void Awake()
    {
        source = GetComponent<AudioSource>();
    }

    /// <summary>
    /// Plays the sound instantly
    /// </summary>
    public void PlaySound()
    {
        source.volume = volume * (Persistence.GetInt(Persistence.key_soundVolume) / 100f);

        source.clip = audioClip;

        source.Play();

        hasPlayed = true;
    }

    /// <summary>
    /// Schedules the sound to play at the given time
    /// </summary>
    public void Schedule(float time)
    {
        source.clip = audioClip;
        scheduled = true;
        scheduleTime = time;
    }

    private void PlayScheduledSound()
    {
        source.PlayScheduled(scheduleTime);
        scheduled = false;
        hasPlayed = true;
    }

    void Update()
    {
        //todo: if the game is paused and a sound is scheduled, delay it??
        if (((!source.isPlaying && !AudioListener.pause) && !scheduled) || source.volume == 0f)
        {
            Destroy(gameObject);

            if (hasPlayed == false)
            {
                Debug.LogError("Sound " + audioClip.name + " has been initialized, but not played!");
            }
        }
        else
        {
            source.volume = volume * (Persistence.GetInt(Persistence.key_soundVolume) / 100f);
        }

        //don't PlayScheduled the sound until just before it plays
        //because otherwise we'll run out of channels and die a painful death
        if (scrMusic.instance.time + scrMusic.instance.startTime + 1 > scheduleTime && scheduled)
        {
            PlayScheduledSound();
        }

    }

    public Sound SetVolume(float volume)
    {
        this.volume = volume;
        return this;
    }

    public Sound SetPitch(float pitch)
    {
        source.pitch = pitch;
        return this;
    }

    public Sound SetRange(float range)
    {
        source.maxDistance = range;
        return this;
    }

    public Sound IgnorePause(bool ignore = true)
    {
        source.ignoreListenerPause = ignore;
        return this;
    }
}
