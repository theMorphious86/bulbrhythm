﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public class scrPauseMenu : SingletonBehaviour<scrPauseMenu>
{
    scrSoundManager sm => scrSoundManager.instance;

    [Header("Components")]
    public GameObject pauseContainer;
    public TextMeshProUGUI pausedText;
    public RenderTexture bgRT;
    public RawImage background;
    public RectTransform cursorRight;
    public RectTransform cursorLeft;

    // pages
    [Header("Pages")]
    [NonSerialized] public MenuPage selectedPage;
    [NonSerialized] public int selectedIndex = 0;
    [NonSerialized] public PauseButton selectedButton;
    public MenuPage pageMain;
    public MenuPage pageSettings;
    public MenuPage pageCredits;

    [Header("Other")]
    public bool paused;
    bool transitioning;
    bool cursorIsMoving;

    void Start()
    {
        RectTransform rt = pausedText.GetComponent<RectTransform>();
        Sequence a = DOTween.Sequence()
            .Append(rt.DORotate(new Vector3(0f, 0f, 10f), 1f).SetEase(Ease.OutSine))
            .Append(rt.DORotate(new Vector3(0f, 0f, 0f), 1f).SetEase(Ease.InSine))
            .Append(rt.DORotate(new Vector3(0f, 0f, -10f), 1f).SetEase(Ease.OutSine))
            .Append(rt.DORotate(new Vector3(0f, 0f, 0f), 1f).SetEase(Ease.InSine))
            .SetLoops(-1, LoopType.Restart).SetUpdate(true);
        rt.DOShakePosition(8, 0.5f, 3, -45, false, false).SetUpdate(true).SetLoops(-1);
        //rt.DOShakeRotation(8, 10f, 1, 45, false).SetUpdate(true).SetLoops(-1);
        Transform cursorLeftSub = cursorLeft.GetChild(0).GetComponent<Transform>();
        Transform cursorRightSub = cursorRight.GetChild(0).GetComponent<Transform>();
        cursorLeftSub.DOLocalMoveX(-5f, 0.75f, true).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.InOutSine).SetUpdate(true);
        cursorRightSub.DOLocalMoveX(5f, 0.75f, true).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.InOutSine).SetUpdate(true);

        pauseContainer.SetActive(false);
        pageMain.gameObject.SetActive(true);
        pageSettings.gameObject.SetActive(false);
        pageCredits.gameObject.SetActive(false);
    }

    void SelectButton(int index, bool instant = false)
    {
        selectedIndex = index;
        SelectButton(selectedPage.buttons[index], instant);
    }

    void SelectButton(PauseButton button, bool instant = false)
    {
        selectedButton = button;

        if (!instant)
        {
            float cursorDur = 0.2f;
            cursorLeft.DOMove(button.leftCursorHolder.position, cursorDur, true).SetUpdate(true).SetEase(Ease.OutBack).OnKill(() => cursorIsMoving = false);
            cursorRight.DOMove(button.rightCursorHolder.position, cursorDur, true).SetUpdate(true).SetEase(Ease.OutBack);
            button.transform.DOComplete();
            button.transform.DOShakePosition(cursorDur, 2).SetUpdate(true);
            cursorIsMoving = true;
        }
        UpdateCursor();

        DoAction(selectedButton.hoverAction);
    }

    void UpdateCursor()
    {
        if (cursorIsMoving) return;
        PauseButton btn = selectedPage.buttons[selectedIndex];
        cursorLeft.position = btn.leftCursorHolder.position;
        cursorRight.position = btn.rightCursorHolder.position;
        cursorLeft.localScale = btn.transform.localScale;
        cursorRight.localScale = btn.transform.localScale;
    }

    void DoAction(string action)
    {
        if (action == null || action == "") return;
        switch (action)
        {
            case "resume":
                scrController.instance.skipInput = true;
                StartTogglePause();
                break;
            case "settings":
                ChangePage(pageSettings, "exitSettings");
                break;
            case "exitSettings":
                ChangePage(pageMain, "settings");
                Persistence.Save();
                break;
            case "credits":
                ChangePage(pageCredits, "exitCredits");
                break;
            case "exitCredits":
                ChangePage(pageMain, "credits");
                break;
        }
    }

    void ChangePage(MenuPage page, string actionToSelect = null)
    {
        selectedPage.gameObject.SetActive(false);

        selectedPage = page;
        page.gameObject.SetActive(true);

        if (actionToSelect != null) SelectButton(page.GetIndexByAction(actionToSelect), true);
        //SelectButton(page.buttons[0], true);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            StartTogglePause();
        }

        if (paused)
        {
            if (!transitioning)
            {
                if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.X) || Input.GetKeyDown(KeyCode.Z))
                {
                    if (selectedButton.enterAction != "") sm.NewSound("sndSelect").IgnorePause().PlaySound();
                    DoAction(selectedButton.enterAction);
                }
                else if (Input.GetKeyDown(KeyCode.RightArrow))
                {
                    DoAction(selectedButton.rightAction);
                    if (selectedButton.valueKey != "") selectedButton.IncrementValue(false);
                }
                else if (Input.GetKeyDown(KeyCode.LeftArrow))
                {
                    DoAction(selectedButton.leftAction);
                    if (selectedButton.valueKey != "") selectedButton.IncrementValue(true);
                }
                else if (Input.GetKeyDown(KeyCode.DownArrow))
                {
                    selectedIndex = selectedIndex + 1 < selectedPage.buttons.Count ? selectedIndex + 1 : 0;
                    SelectButton(selectedPage.buttons[selectedIndex]);
                    sm.NewSound("sndHoverBlip").IgnorePause().PlaySound();
                }
                else if (Input.GetKeyDown(KeyCode.UpArrow))
                {
                    selectedIndex = selectedIndex - 1 >= 0 ? selectedIndex - 1 : selectedPage.buttons.Count - 1;
                    SelectButton(selectedPage.buttons[selectedIndex]);
                    sm.NewSound("sndHoverBlip").IgnorePause().PlaySound();
                }
            }
            UpdateCursor();
        }
    }

    void Hover(PauseButton button)
    {

    }

    WaitForEndOfFrame frameEnd = new WaitForEndOfFrame();
    IEnumerator TogglePauseCo()
    {
        yield return frameEnd; // wait for the frame to be drawn so the screenshot can be created
        yield return true; // wait one more frame so the game still has time to display the frame and doesn't just display a black screen
        TogglePause();
        yield break;
    }

    void StartTogglePause()
    {
        if (transitioning) return;
        if (paused)
        {
            DoTransition(false);
        }
        else
        {
            StartCoroutine(TogglePauseCo());
            DoTransition(true);
        }
    }

    public void TogglePause()
    {
        paused = !paused;
        transitioning = false;

        // IMPORTANT!!
        Time.timeScale = paused ? 0 : 1;
        AudioListener.pause = paused ? true : false;

        pauseContainer.SetActive(paused ? true : false);

        if (paused)
        {
            // select first page
            selectedPage = pageMain;
            selectedIndex = 0;
            SelectButton(selectedPage.buttons[selectedIndex], true);

            Camera cam = scrCamera.instance.bgCam;
            cam.targetTexture = bgRT;
            RenderTexture.active = bgRT;

            cam.Render();

            Texture2D img = new Texture2D(bgRT.width, bgRT.height);
            img.ReadPixels(new Rect(0, 0, bgRT.width, bgRT.height), 0, 0);
            img.Apply();
            img.filterMode = FilterMode.Point;
            cam.targetTexture = null;
            RenderTexture.active = cam.targetTexture;

            background.texture = img;
        }
    }

    void DoTransition(bool pause)
    {
        transitioning = true;
        Vector3 oldScale = !pause ? Vector3.one : Vector3.zero;
        Vector3 newScale = pause ? Vector3.one : Vector3.zero;
        Ease newEase = pause ? Ease.OutBack : Ease.InBack;
        Color newFadeCol = pause ? (Color)new Color32(0x6F, 0x6F, 0x6F, 0xFF) : Color.white;
        pageMain.UpdateButtons();

        // do cool animation whoa
        for (var i = 0; i < pageMain.buttons.Count; i++)
        {
            var time = i * 0.075f;
            Transform tr = pageMain.buttons[i].transform;
            DOTween.Kill(tr);

            tr.localScale = oldScale;
            tr.DOScale(newScale, 0.2f).SetEase(newEase).SetDelay(time).SetUpdate(true);

            //TextMeshProUGUI tmp = pageMain[i].text;
            //tmp.DOFade(0f, 0f).SetUpdate(true);
            //tmp.DOFade(1f, 0.2f).SetDelay(time).SetUpdate(true);
        }

        // take the screenshot
        //pausedText.GetComponent<RectTransform>().DOShakeScale(1f, 0.25f, 5, 90, true).SetUpdate(true);
        DOTween.Kill(pausedText.GetComponent<RectTransform>());
        //pausedText.GetComponent<RectTransform>().localScale = oldScale;
        //pausedText.GetComponent<RectTransform>().DOScale(newScale, 0.5f).SetEase(newEase).SetUpdate(true);
        pausedText.GetComponent<RectTransform>().DOLocalMoveY(pause ? 56f : 120, 0.4f).SetEase(pause ? Ease.OutBack : Ease.InCubic).SetUpdate(true);

        if (!pause)
        {
            background.DOFade(0f, 0.25f).SetDelay(0.5f).SetUpdate(true);
        }
        else
        {
            background.DOFade(0f, 0f).SetUpdate(true);
            background.DOFade(1f, 0.25f).SetUpdate(true);
        }

        DOTween.Sequence().AppendInterval(0.75f).SetUpdate(true).OnComplete(() => { if (!pause) TogglePause(); });
    }
}
