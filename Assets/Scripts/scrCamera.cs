﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scrCamera : SingletonBehaviour<scrCamera>
{
    Camera _cam;
    public Camera cam
    {
        get
        {
            if (_cam == null) _cam = GetComponent<Camera>();
            return _cam;
        }
    }

    Camera _bgCam;
    public Camera bgCam
    {
        get
        {
            if (_bgCam == null) _bgCam = GetComponentInChildren<Camera>();
            return _bgCam;
        }
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
