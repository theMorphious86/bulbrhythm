﻿using UnityEngine;
using System.Collections;

public class scrScroller : MonoBehaviour
{
    //the idea: have two of these. Jump by two screens.
    //this also needs to incorporate parallax though, since must be in relation to a camera

    float jumpamount;
    public float scrollspeed;
    public float jumpOffset;
    float curroffset;
    SpriteRenderer spr;

    public Transform cameraTransform;
    Vector3 startpos;

    void Start()
    {
        spr = GetComponent<SpriteRenderer>();
        jumpamount = spr.sprite.bounds.max.x - spr.sprite.bounds.min.x;
        startpos = gameObject.transform.position;
        cameraTransform = Camera.main.transform;
        if (jumpOffset != 0) jumpamount = jumpOffset;
    }

    void Update()
    {
        //parallax stuff

        //transform.position = startpos + (objCam.transform.position - posCamAtStart) * multiplier_x;
        Vector3 _vec = Vector3.zero;
        _vec.x = startpos.x + cameraTransform.position.x + curroffset;
        _vec.y = startpos.y + cameraTransform.position.y + curroffset;
        _vec.z = startpos.z;
        transform.position = _vec;
        Vector3 tmp = transform.position;
        tmp.z = startpos.z;
        transform.position = tmp; //wtf is this

        curroffset += scrollspeed * Time.deltaTime;
        if (curroffset < -jumpamount)
        {
            curroffset = 0;
        }


    }
}
